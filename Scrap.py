import requests
from bs4 import BeautifulSoup
import csv

HEADERS = ['Event Name', 'Date And Time', 'Location', 'Refund Policy', 'Description' ,'Organizer Name', 'Image URL']
scrape_rows = [HEADERS]

#define url parameters
SEARCH_LOCATION = 'canada--toronto'
SEARCH_KEYWORD = 'party'
BASE_URL = 'https://www.eventbrite.ca/d/{}/{}/?page={}'
OUTPUT_FILENAME = 'eventbrite-scrape-results.csv'

# logic to extract date
def get_date_time(str):
    if str:
        split_str = str.split('\n')
        return split_str[0] + ' ' + split_str[1]
    return ''


# logic to extract address
def get_location(str):
    if str:
        split_str = str.split('\n')
        addr = ''
        for item in split_str:
            item_stripped = item.strip()
            addr = addr + item_stripped + ' '
            if not item:
                return addr
    return ''


# logic to parse description
def get_description(str):
    if str:
        split_str = str.split('\n')
        desc = ''

        for line in split_str:
            if line.strip() == 'Read more':
                return desc
            if not line.strip():
                continue
            desc = desc + line.strip() + '\n'

        return desc
    return ''


# logic to get organizer
def get_organizer(str):
    if str:
        split_arr = str.split(' ', 1)
        return split_arr[1].strip()
    return ''


# logic to scrape details from an event
def scrape_list_event(url):
    print('[eventbrite] Scraping event: {}'.format(url))
    response = requests.get(url)
    soup = BeautifulSoup(response.content, 'lxml')

    event_name_el = soup.find('h1', {'class': 'listing-hero-title'})
    event_name = event_name_el.get_text().strip() if event_name_el else url

    date_time_el = soup.find('time', {'data-automation': 'event-details-time'})
    date_time_str = date_time_el.get_text().strip() if date_time_el else ''
    date_time = get_date_time(date_time_str)

    location_el = soup.find_all('div', {'class': 'event-details__data'})
    location_str = location_el[1].get_text().strip() if len(location_el) > 0 else ''
    location = get_location(location_str).strip()

    refund_policy_el = soup.find_all('div', {'data-automation': 'refund-policy'})
    refund_policy = refund_policy_el[0].get_text().strip() if len(refund_policy_el) > 0 else ''

    description_el = soup.find_all('div', {'class': 'has-user-generated-content'})
    description_str = description_el[0].get_text().strip() if len(description_el) > 0 else ''
    description = get_description(description_str).strip()

    organizer_el = soup.find_all('a', {'class': 'listing-organizer-name'})
    organizer_str = organizer_el[0].get_text().strip() if len(organizer_el) > 0 else ''
    organizer = get_organizer(organizer_str)

    picture_el = soup.find_all('picture')
    url = picture_el[0]['content'] if picture_el else ''

    return [event_name, date_time, location, refund_policy, description, organizer, url]

# count variable that determines the page
count = 1
#loop that iterates search result pages
while True:
    print('[eventbrite] Scraping page {}'.format(count))
    #we obtain a page with search results and parse it
    response = requests.get(BASE_URL.format(SEARCH_LOCATION, SEARCH_KEYWORD, count))
    soup = BeautifulSoup(response.content, 'lxml')

    search_result_uls = soup.find_all('ul', {'class': 'search-main-content__events-list'})

    #exit condition. break from loop once all pages are scraped
    if not search_result_uls:
        print('[eventbrite]: All pages scraped.')
        break

    print('[eventbrite] Extracting event links')
    # list of events in a given page
    search_result_lis = search_result_uls[0].find_all('li')

    # we iterate event list, extract each event's URL and pass it to scrape_list_event to scrape detailed info
    for li in search_result_lis:
        try:
            event_url = li.find('a', {'class': 'eds-media-card-content__action-link'})['href'].strip()
            scrape_row = scrape_list_event(event_url)
            scrape_rows.append(scrape_row)
        except Exception as e:
            print('[eventbrite][error] Event details page does not conform to the expected format. Moving on to next link')

    count = count + 1

# write to csv
with open(OUTPUT_FILENAME, 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerows(scrape_rows)

print('[eventbrite]: Scraping complete. Script will now exit')